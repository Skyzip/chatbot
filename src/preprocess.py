import re
import unicodedata


# Converts the unicode file to ascii
def unicode_to_ascii(unicode_str: str):
    return ''.join(c for c in unicodedata.normalize('NFD', unicode_str)
                   if unicodedata.category(c) != 'Mn')


def preprocess_sentence(sentence: str):
    ascii_sentence = unicode_to_ascii(sentence.lower().strip())

    # creating a space between a word and the punctuation following it
    preprocessed = re.sub(r'([?.!,¿])', lambda match: ' ' + match.group() + ' ', ascii_sentence)
    # kill multiple spaces
    preprocessed = re.sub(r'[ ]+', ' ', preprocessed)

    preprocessed = preprocessed.strip()

    # adding a start and an end token to the sentence
    # so that the model know when to start and stop predicting.
    preprocessed = '<start> ' + preprocessed + ' <end>'
    return preprocessed


processed_sentence = preprocess_sentence("Hello there my bóy! Good to see you, too!")
print(processed_sentence)
