===================
The chatbot project
===================


A chatbot. With it, you can have at least one friend that talks to you.

It never gets hungry, nor does it sleep. It is the perfect pet.

Prerequisites
=============

 - pip install tensorflow
